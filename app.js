const Router = require('./router/router');
const Log4js = require('log4js');
const Config = require('./config/config');

function initRouter() {
    router = new Router();
    router.init();
};

//function initLogger() {
//    Log4js.configure(Config.logger);
//};

function main(){
    //initLogger();
    initRouter();
};

main();