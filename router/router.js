const UserController = require("../controller/user");
const express = require('express');
const Config = require("../config/config");
const log4js = require('log4js');
var morgan = require('morgan')
var path = require('path')
var rfs = require('rotating-file-stream')

function Router() {
    
    this.app = express();

    this.initRouter = function() {
        this.app.get('/', (new UserController()).getUsers);
        this.app.listen(Config.port, () => {
            console.log(`Example app listening at http://localhost:${Config.port}`)
        });
    };

    this.initAccessLog = function() {
        var accessLogStream = rfs.createStream(Config.logger.access.name, {
            interval: Config.logger.access.interval, // rotate daily
            path: Config.logger.access.path
        });
        this.app.use(morgan('combined', { stream: accessLogStream }))
    };

    this.init = function() {
        this.initAccessLog();
        this.initRouter();
    };
};

module.exports=Router;