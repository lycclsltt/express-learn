var Config = {
    'port' : 3000,
    'db' : {
        'default' : {
            'host' : '127.0.0.1',
            'port' : 3306,
            'user' : 'root',
            'password' : '',
            'dbname' : 'nation'
        }
    },
    'logger' : {
        'access' : {
            'name' : 'access.log',
            'interval' : '1d',
            'path' : 'logs'
        },
    },
};

module.exports = Config;